velocity-pressure-stress.pdf: velocity-pressure-stress.tex velocity-pressure-stress.bib
	pdflatex $<
	bibtex $(basename $< .tex)
	pdflatex $<
	pdflatex $<

all: velocity-pressure-stress.pdf
