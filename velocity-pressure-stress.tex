\documentclass{article}

\usepackage{amsmath}
%\usepackage{amsfonts}
\usepackage{amsthm}
%\usepackage{amssymb}
%\usepackage{mathrsfs}
%\usepackage{fullpage}
%\usepackage{mathptmx}
%\usepackage[varg]{txfonts}
\usepackage{color}
\usepackage[charter]{mathdesign}
\usepackage[pdftex]{graphicx}
%\usepackage{float}
%\usepackage{hyperref}
%\usepackage[modulo, displaymath, mathlines]{lineno}
%\usepackage{setspace}
%\usepackage[titletoc,toc,title]{appendix}
\usepackage{natbib}

%\linenumbers
%\doublespacing

\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem*{exm}{Example}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\newtheorem*{lem}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{cor}{Corollary}

\newcommand{\argmin}{\text{argmin}}
\newcommand{\ud}{\hspace{2pt}\mathrm{d}}
\newcommand{\bs}{\boldsymbol}
\newcommand{\PP}{\mathsf{P}}

\title{Trust region solvers for mixed variational formulations of shear-thinning flows}
\author{Daniel Shapero}
\date{}

\begin{document}

\maketitle

\section{Introduction}

The flow of glaciers and ice sheets on long length and time scales (> 100 m, > 7 days) is described well by the incompressible Stokes equations \citep{greve2009dynamics}.
These are a system of PDE for the fluid velocity $u$, pressure $p$, and deviatoric stress tensor $\tau$:
\begin{align}
    \nabla\cdot\tau - \nabla p + \rho g & = 0 \\
    \nabla\cdot u & = 0.
\end{align}
The first equation describes the balance of viscous forces, pressure, and gravity, while the second equation states that the flow is incompressible.
To complete the description of the system, however, we need to know what the \emph{constitutive} relation is between the \emph{strain rate} tensor
\begin{equation}
    \dot\varepsilon \equiv \frac{1}{2}\left(\nabla u + \nabla u^*\right),
\end{equation}
i.e. the symmetrized gradient of the velocity field, and the deviatoric stress tensor.
For an incompressible Newtonian fluid, the deviatoric stress tensor and the strain rate tensor are linearly proportional to each other:
\begin{equation}
    \tau = 2\mu\dot\varepsilon
    \label{eq:newtonian-constitutive-relation}
\end{equation}
where $\mu$ is the viscosity coefficient.
Polycrystalline ice is not a Newtonian fluid.
Instead, the strain rate tensor $\dot\varepsilon$ is roughly a power-law function of the deviatoric stress $\tau$:
\begin{equation}
    \dot\varepsilon = A|\tau|^{n - 1}\tau
    \label{eq:glen-flow-law}
\end{equation}
where $A$ is a temperature-dependent \emph{rate factor}, $n \approx 3$ is the \emph{flow law exponent}, and $|\cdot|$ is the second invariant of a rank-2 tensor.
For realistic glacier flows, equation \eqref{eq:glen-flow-law} is an approximation from which there may be departures.
The flow law exponent was originally obtained through empirical fits to laboratory experiments that could not replicate the stress and strain rate values of real glacier flows.
Some works in the literature have proposed other values of the flow law exponent.
The constitutive relation can also become anisotropic under sustained strain along a preferential axis.
In that case it would be more appropriate to make the scalar rate factor $A$ into a rank-4 tensor.
Nonetheless, a majority of published works in the glaciological literature use the Glen flow law as written above.

The form of Glen's flow law in equation \eqref{eq:glen-flow-law} describes the strain rate tensor as a function of the stress tensor, but to substitute it into the Stokes equations we instead require the inverse -- stress in terms of strain rate.
Equation \eqref{eq:glen-flow-law} can easily be inverted to yield
\begin{equation}
    \tau = A^{-\frac{1}{n}}|\dot\varepsilon|^{\frac{1}{n} - 1}\dot\varepsilon.
    \label{eq:glen-flow-law-inverse}
\end{equation}
Comparison of the last equation with the Newtonian constitutive law \eqref{eq:newtonian-constitutive-relation} shows that the viscosity of ice is proportional to the strain rate tensor to the power of $\frac{1}{n} - 1$.
Since $n \approx 3$, the viscosity goes to zero at large strain rates and, most important for what follows, to infinity around zero strain rate.

The nonlinear Stokes equations can be expressed concisely through an action or minimization principle \citep{dukowicz2010consistent}.
The viscous energy dissipation term in the action functional for the nonlinear Stokes equations is
\begin{equation}
    J_{\text{viscous}} = \frac{n}{n + 1}\int_\Omega A^{-\frac{1}{n}}|\dot\varepsilon(u)|^{\frac{1}{n} + 1}\ud x.
\end{equation}
The singularity of the viscosity around zero strain rate is also reflected in the behavior of the action functional.
The Hessian of the action acting on test vector fields $v$, $w$ can be written as
\begin{equation}
    \langle \ud^2J(u)\cdot v, w\rangle = \int_\Omega A^{-\frac{1}{n}}|\dot\varepsilon(u)|^{\frac{1}{n} - 1}\mathscr{C}\dot\varepsilon(v) : \dot\varepsilon(w)\ud x
\end{equation}
where $\mathscr{C}$ is a rank-4 tensor that is bounded above and below.
(Calculating this tensor explicitly is rather involved, but unimportant for the discussion that follows. See \citet{isaac2015solution} for details.)
For any power-law relation where $n > 1$, \textbf{the kernel of the second derivative operator has an infinite singularity as the strain rate tensor $\dot\varepsilon(u)$ approaches 0.}
Moreover, real ice sheets do exhibit zero strain rate at the bases of domes and divides.
The singularity of the viscosity gives rise to an effect called \emph{Raymond arches}.
The observation of Raymond arches in radar measurements was one of the strongest pieces of evidence for nonlinear glacier flow \citep{gillet2011insitu}.

The singularity that the constitutive relation implies is a huge problem for numerical analysis.
Newton's method for optimization requires the ability to calculate both the first and second derivative of the action functional.
The singularity of the second derivative for this particular problem rules out using one of the most effective solution methods.
The conventional approach is to introduce a perturbation factor $\dot\varepsilon_0$ to remove the singularity.

\textbf{In this paper, we will describe an alternative formulation of shear-thinning flow problems and a solution strategy with favorable properties in the face of these singularities.}
This alternative form of the physics reintroduces the stress tensor as a variable in the problem.
The resulting system of equations has more unknowns, but is easier to solve reliably using \emph{trust region methods} \citep{conn2000trust}.
Calculating the stress tensor is often necessary both for analysis of the resulting flow and as an input to mathematical models of other glaciological processes, such as damage mechanics.

In the following, we will rely exclusively on the minimization form of the physics models we study.
Not all PDEs models have minimization principles, but when they do it implies several useful properties that can guide what numerical methods we use.
First, the second derivative of the action is a symmetric operator by Clairaut's theorem.
To solve symmetric systems we can use specialized methods, such as MINRES, that require less memory than their general counterparts, such as GMRES.
Next, the action functional for the Stokes equations is convex with respect to $u$, which is significant in two respects.
For solving the Stokes equations numerically, convexity implies that the velocity block of the operator is positive-definite.
This property implies favorable stability estimates for the solution in terms of the input data, c.f. theorem 4.2.3 in \citet{boffi2013mixed}.
Additionally, it means we can use solvers and preconditioners such as the conjugate gradient or multigrid methods that are specialized for positive-definite problems.
From the perspective of thermodynamics, convexity implies that the physical trajectory maximizes the local free energy dissipation rate \citep{edelen1972nonlinear}.
Finally, the action functional is more concise to write down than the equivalent system of nonlinear equations.

\begin{table}[h]
    \begin{tabular}{l|l}
        Symbol & Meaning \\
        \hline
        $h$ & thickness \\
        $b$ & bed elevation  \\
        $s$ & surface elevation  \\
        $u$ & velocity \\
        $\dot\varepsilon$ & strain rate, $\frac{1}{2}(\nabla u + \nabla u^\top)$\\
        $A$ & rheology coefficient \\
        $M$ & membrane stress tensor \\
        $C$ & bed friction coefficient \\
        $\nu$ & unit outward normal \\
        $J$ & action functional \\
        $\Omega$ & spatial domain \\
        $\Gamma$ & calving terminus
    \end{tabular}
    \caption{Mathematical symbols}
    \label{tab:mathematical-symbols}
\end{table}

\begin{table}[h]
    \begin{tabular}{l|l}
        Symbol & Meaning \\
        \hline
        $n$ & Glen's flow law exponent ($\approx 3$) \\
        $m$ & Weertman sliding law exponent ($\approx 3$) \\
        $\rho_I$ & ice density \\
        $\rho_W$ & seawater density \\
        $g$ & gravitational acceleration
    \end{tabular}
    \caption{Physical constants}
    \label{tab:physical-constants}
\end{table}

The Stokes equations are the most physically faithful but complex model used in glaciology.
Several simpler models can be derived via perturbation theory \citep{greve2009dynamics, dukowicz2010consistent}.
First, the ratio of the vertical to horizontal length scales in a typical glacier flow problem is usually on the order of 1/20 or less.
Expanding the Stokes equations in the aspect ratio $\delta = H / L$ gives a simpler system from which the vertical velocity and pressure have been eliminated.
This system is called the \emph{first-order} or \emph{Blatter-Pattyn} approximation.
Next, in fast-flowing regions of the ice sheet, the horizontal shear and extensional stresses are much larger than the vertical shear stresses.
With this approximation, we can instead work with the depth-averaged velocity, giving a 2D elliptic system called the \emph{shallow stream equations}.
The action functional for the primal form of the shallow stream equations is
\begin{equation}
    J(u) = \int_\Omega\left(\frac{2n}{n + 1}A^{-\frac{1}{n}}|\mathscr{C}\dot\varepsilon(u)|^{\frac{1}{n} + 1} + C|u|^{\frac{1}{m} + 1} + \rho gh\nabla s\cdot u\right)\ud x.
    \label{eq:ssa-action}
\end{equation}
(See table \ref{tab:mathematical-symbols} for definitions of variables and table \ref{tab:physical-constants} for definitions of physical constants.)
Let $I$ be the 2 $\times 2$ identity matrix; the rank-4 tensor $\mathscr{C}$ acts on rank-2 tensors by
\begin{equation}
    \mathscr{C}\dot\varepsilon = \frac{1}{2}(\dot\varepsilon + \mathrm{tr}(\dot\varepsilon)I)
\end{equation}
where $\mathrm{tr}$ denotes the trace of a matrix.
We are assuming some form of power-law sliding with an exponent $m$.
This is a common assumption which we also adopt for simplicity of exposition, although the current understanding of glacier sliding favors a slightly more complicated relation \citep{minchew2020toward}.

The method that we propose incorporates several distinct ideas.
Rather than proceed directly to studying the shallow stream equations, we will take a slight detour through models for groundwater flow.
These models are simpler than the shallow stream equations and will serve to introduce each mathematical concept by itself.
The organization of the paper is as follows.
In section \S\ref{sec:poisson}, we will describe the mixed form of the Poisson equation, which can be used to describe the hydraulic potential in groundwater hydrology.
In section \S\ref{sec:p-laplace-thickening}, we will describe a nonlinear generalization of the Poisson equation that exhibits many of the same properties as shear-thinning or shear-thickening flow depending on the parameters.
We will then show how, in the shear-thickening case, we can effectively solve this problem using \emph{trust region methods}.
In section \S\ref{sec:p-laplace-thinning}, we will then study the shear-thinning case of the nonlinear Poisson equation.
By instead switching to the mixed form introduced in section \S\ref{sec:poisson}, we \emph{invert} the constitutive equation and turn any exponents in the problem from less than 2 to greater than 2.
We can then attack the mixed form of the problem using trust region methods.
Finally, in section \S\ref{sec:shallow-stream}, we will apply all of these ideas to the shallow stream equations.


\section{The mixed Poisson problem}\label{sec:poisson}

The generalized Poisson equation
\begin{equation}
    -\nabla\cdot k\nabla\phi = f
\end{equation}
for the scalar field $\phi$ describes many phenomena throughout the sciences and engineering.
In particular, if $k$ is the hydraulic conductivity of a porous medium, the hydraulic potential $\phi$ solves the Poisson equation.
The solution of this PDE is also a minimizer of the action functional
\begin{equation}
    J(\phi) = \int_\Omega\left(\frac{1}{2}k\nabla\phi\cdot\nabla\phi - f\phi\right)\ud x.
\end{equation}
The Poisson equation is the prototypical example of a problem with a minimization principle.

In groundwater hydrology, the potential $\phi$ itself is not so much the quantity of interest as its gradient, the fluid velocity
\begin{equation}
    u = -k\nabla\phi.
    \label{eq:linear-poisson-constitutive-relation}
\end{equation}
The mixed form of the Poisson equation solves for both the potential and velocity at the same time:
\begin{align}
    k^{-1}u + \nabla\phi & = 0 \\
    \nabla\cdot u & = f.
\end{align}
The mixed form of the Poisson equation also has a minimization principle.
The Lagrangian is
\begin{equation}
    L(u, \phi) = \int_\Omega\left\{\frac{1}{2}k^{-1}u\cdot u - \phi\cdot\left(\nabla\cdot u - f\right)\right\}\ud x
    \label{eq:mixed-poisson-lagrangian}
\end{equation}
and the solution of the mixed problem is a critical point of this Lagrangian.
Informally, we now seek to minimize $L$ with respect to $u$, but subject to the constraint that $\nabla\cdot u = f$.
The potential $\phi$ now acts like a Lagrange multiplier that enforces this constraint.

The peculiar thing about the mixed formulation is that it \textbf{inverts the constitutive relation}.
We start by deriving the Poisson equation with the constitutive equation \eqref{eq:linear-poisson-constitutive-relation} for $u$ in terms of $\phi$.
For $u$ and $\phi$ to be a critical point of the Lagrangian $L$ in equation \eqref{eq:mixed-poisson-lagrangian}, we need for $\partial L/\partial u = 0$, which instead gives us the relation
\begin{equation}
    \nabla\phi = -k^{-1}u.
\end{equation}
The two equations are completely equivalent.
But when we extend to nonlinear problems, the two have singularities of opposite type.
This feature will be crucial to our final approach.

The primal form of the Poisson equation can be discretized using more or less any choice of finite element basis, so long as the mesh geometry stays of high quality under refinement.
The mixed form, however, requires that either (1) the basis functions satisfy the \emph{Ladyzhenskaya-Babu\v{s}ka-Brezzi} or LBB conditions, or (2) the problem is perturbed or penalized to circumvent them \citep{boffi2013mixed}.


\section{The nonlinear Poisson equation and trust region methods}\label{sec:p-laplace-thickening}

Both the Poisson equation and its mixed form are linear, and so in the worst case they can be solved using direct factorization.
The Poisson equation can be generalized to a nonlinear variant:
\begin{equation}
    J(\phi) = \int_\Omega\left(\frac{1}{p}\ell^p|\nabla\phi|^p - f\phi\right)\ud x
\end{equation}
where $p$ is some exponent.
When $p = 2$, we recover the usual linear Poisson equation.
For $1 < p < \infty$, this problem is strictly convex and thus has a solution if we assume that $f$ and the boundary conditions obey some regularity properties.

In the nonlinear cases, however, the linearization has singularities around any point where $\nabla\phi = 0$.
To see this, we can look at the second derivative of $J$:
\begin{equation}
    \langle\ud^2J(\phi)\cdot\theta,\psi\rangle = \int_\Omega \ell^p|\nabla\phi|^{p - 2}D(\nabla\phi)\nabla\theta\cdot\nabla\psi\ud x,
\end{equation}
where the tensor $D$ is defined as
\begin{equation}
    D(\nabla\phi) = I + (p - 2)\frac{\nabla\phi\cdot\nabla\phi^*}{|\nabla\phi|^2}.
\end{equation}
The eigenvalues of $D$ are 1 and $p - 1$, so as long as $p > 1$ we can consider this quantity to be bounded above and below.
The critical part, however, is the $|\nabla\phi|^{p - 2}$ term in the linearization.
If $p < 2$, then the linearization has an \emph{infinite} degeneracy around any point where $\nabla\phi = 0$, whereas if $p > 2$, the linearization has a \emph{zero} degeneracy.
The linearization is a degenerate elliptic operator in either case; the exponent dictates what type of degeneracy.

To solve nonlinear problems, we would like to use some kind of Newton method.
At each iteration, we improve some candidate guess $\phi$ according to the rule
\begin{equation}
    \phi \leftarrow \phi - \alpha\ud^2J(\phi)^{-1}\ud J(\phi)
\end{equation}
for some real damping factor $\alpha$ that we compute through, say, a line search.
While the theory of convex analysis guarantees that a solution exists, the fact that $\ud^2J$ can be a degenerate operator poses challenges because we can no longer use one of the most effective strategies for approximating this solution.

For shear-thinning flows, we are more interested in cases with infinite degeneracy.
Nonetheless, we can develop some intuition for a good solution strategy by considering the $p > 2$ case first, which instead has a zero degeneracy.

The key idea is the application of \emph{trust region methods} instead of line search methods \citep{conn2000trust}.
Rather than directly try to apply the inverse of the Hessian operator, at each step we instead compute a function $\psi$ that minimizes the quadratic functional
\begin{equation}
    \hat J(\psi) = J(\phi) + \langle \ud J(\phi), \psi\rangle + \frac{1}{2}\langle\ud^2J(\phi)\psi, \psi\rangle
\end{equation}
subject to the added constraint that
\begin{equation}
    \|\psi\| \le \Delta
\end{equation}
where $\Delta$ is the \emph{trust region radius}.
An added subtlety is that we also have a choice of what norm to use to define the shape of the trust region.
We'll assume that the norm is induced by some positive-definite, self-adjoint operator $G : Q \to Q^*$, where $Q$ is the function space that $\phi$ lives in.
We can then enforce the constraint that $\|\psi\|_G \le \Delta$ by adding a scalar Lagrange multiplier $\lambda$, so that the trust region subproblem can instead be written as finding a critical point of
\begin{equation}
    \hat L(\psi, \lambda) = J(\phi) + \langle \ud J(\phi), \psi\rangle + \frac{1}{2}\langle(\ud^2J(\phi) + \lambda G)\psi, \psi\rangle.
\end{equation}
While $\ud^2J(\phi)$ might have a zero degeneracy, as long as $G$ is non-degenerate, we can guarantee that solving the system
\begin{equation}
    \left(\ud^2J(\phi) + \lambda G\right)\psi = -\ud J(\phi)
\end{equation}
is possible for $\lambda > 0$.
The trust region constraint acts like an implicit regularization of the Newton system.
The radius $\Delta$ is then updated at each step according to how much the objective decreased compared to the expected decrease based on the second-order Taylor expansion.

To complete the specification of the algorithm, we need to decide what operator $G$ to use.
The nonlinear Poisson problem is posed over the Sobolev space $W_1^p(\Omega)$ consisting of functions that are $p$-th power integrable along with all of their first partial derivatives.
A critical factor to take into account is whether $G$ is continuous on the function space that $\phi$ lives in.
For the $p > 2$ case, the operator defined by
\begin{equation}
    \langle G\psi, \chi\rangle = \int_\Omega\left(\psi\chi + \ell^2\nabla\phi\cdot\nabla\chi\right)\ud x
\end{equation}
is continuous on $W_1^p(\Omega)$ and, more importantly, is non-degenerate.
We could try to get even more creative and adapt the trust region shape to the Hessian at the current iterate.
For example, $\overline{|\nabla\phi|^2}^{\frac{1}{2}}$ denote the root-mean square gradient of $\phi$:
\begin{equation}
    \overline{|\nabla\phi|^2} = \frac{1}{\mathrm{vol(\Omega)}}\int_\Omega|\nabla\phi|^2\ud x
    \label{eq:helmholtz-operator}
\end{equation}
which again exists because $p > 2$.
The operator
\begin{equation}
    \langle G\psi, \chi\rangle = \int_\Omega\left\{\psi\chi + \ell^p\overline{|\nabla\phi|^2}^{\frac{p - 2}{2}}\left(I + (p - 2)\frac{\nabla\phi\cdot\nabla\phi^*}{|\nabla\phi|^2}\right)\nabla\psi\cdot\nabla\chi\right\}\ud x,
\end{equation}
comes from replacing the term $|\nabla\phi|^{p - 2}$ in the true Hessian operator with a spatial average.
This operator is non-degenerate but much more closely mimics the behavior of the Hessian than the Helmholtz operator in equation \eqref{eq:helmholtz-operator}.

We implemented the trust region method using several different choices of operator $G$.
We solved the nonlinear Poisson equation in a disk using constant input data that result in an analytical solution.
See the supplemental material for the source code.
The results converge with the expected order of accuracy, see figure \textcolor{red}{add this in.}


\section{The mixed nonlinear Poisson equation with trust regions}\label{sec:p-laplace-thinning}

For the $p > 2$ case, we were able to solve the nonlinear Poisson equation using the trust region method.
The implicit regularization that the trust region method applies effectively removes the degeneracy in all of the subproblems.
In the $p < 2$ case, however, we have an infinite degeneracy and using a trust region method directly will not alleviate this.

Instead, we can switch to the mixed form of the problem.
Like the mixed form of the linear Poisson equation, the mixed form of the nonlinear Poisson equation also inverts the relation between the potential $\phi$ and the velocity $u$, but with the new effect of change the exponents.
Let $q \in \mathbb{R}$ such that $p^{-1} + q^{-1} = 1$.
Then the Lagrangian for the mixed form of the nonlinear Poisson equation is
\begin{equation}
    L(u, \phi) = \int_\Omega\left\{\frac{1}{q}\ell^{-q}|u|^q - \phi\cdot\left(\nabla\cdot u - f\right)\right\}\ud x.
\end{equation}
Note that if $p < 2$, then $q > 2$.
While the primal problem has an infinite degeneracy wherever $\nabla\phi = 0$, the dual problem has a zero degeneracy at any point where $u = 0$.
We can use the trust region method again, only for the dual problem instead of the primal.

The trust region method for constrained optimization problems requires a few modifications compared to the unconstrained case.
Here we follow the approach outlined in \citet{heinkenschloss2014matrix}.
\textcolor{red}{Finish this...}

We also have to be mindful of what function space $u$ lives in and what kind of norm we can use to define the trust region shape.
For the nonlinear Poisson equation, the potential $\phi$ lives in $H^1$ and so we could use a norm that was equivalent to the norm of that space.
When we solve the mixed problem, however, we assume only that the divergence of $u$ is $q$-th power integrable, not that the full gradient of $u$ is.
Consequently, the simplest trust region shape can be defined by the operator
\begin{equation}
    \langle Gv, w\rangle = \int_\Omega\left\{v\cdot w + \ell^2(\nabla\cdot v)\cdot(\nabla\cdot w)\right\}\ud x.
\end{equation}
We can once again try to adapt $G$ to the true Hessian by instead using
\begin{equation}
    \langle Gv, w\rangle = \int_\Omega\left\{\overline{|u|^2}^{\frac{q - 2}{2}}\left(I + (q - 2)\frac{u\cdot u^*}{|u|^2}\right)v\cdot w + \ell^2(\nabla\cdot v)\cdot (\nabla\cdot w)\right\}\ud x.
\end{equation}


\section{The mixed shallow stream equations}\label{sec:shallow-stream}

Finally, we will apply the trust region to the mixed form of the shallow stream equations.
The mixed form of the shallow stream equations includes the (rank-2) membrane stress tensor $M$ as an unknown in addition to the velocity, which now acts as a Lagrange multiplier enforcing the momentum balance equation.
The presence of the basal friction, however, presents an additional complication.
We will address this by introducing yet a third unknown $\tau_b$ representing the basal shear stress.
The Lagrangian for the dual problem to minimizing the functional in equation \eqref{eq:ssa-action} is
\begin{align}
    & L(M, \tau_b, u) =
    \\ & \quad\int_\Omega\left\{\frac{2^{-n}}{n + 1}hA|\mathscr{C}^{-1}M|^{n + 1} + \frac{1}{m + 1}C^{-m}|\tau_b|^{m + 1} + u\cdot\left(\nabla\cdot hM + \tau_b - \rho gh\nabla s\right)\right\}\ud x. \nonumber
\end{align}
Since both $m$ and $n$ are greater than 2, the Hessian for this problem has only zero degeneracies and is thus amenable to solution by trust region methods.

We also have to once again make a decision about what norm will define the shape of the trust region.
The membrane stress tensor lives in the function space $W_{\text{div}}^{n + 1}(\Omega)$ of symmetric tensor fields whose divergences are $n + 1$-power integrable.
Consequently, we can use the norm in $W_{\text{div}}^2$:
\begin{equation}
    \langle GM, N\rangle = \int_\Omega\left(M : N + h^2(\nabla\cdot M)\cdot(\nabla\cdot N)\right)\ud x.
\end{equation}
Note that we've used the thickness $h$ to define a natural length scale.


\section{Conclusion}

Several studies have explored using the velocity-pressure-stress or three-field formulation of the nonlinear Stokes equations \citep{manouzi2001mixed, ervin2008dual, codina2009finite, farhloul2017dual}.
These works have focused on things like finding what types of finite elements are LBB-stable and on deriving a priori estimates for the solution norm, but relatively few have examined solution strategies for the discretized equations.
To our knowledge, this is the first work in the literature to explore solution strategies for these types of problems.

\pagebreak

\bibliographystyle{plainnat}
\bibliography{velocity-pressure-stress.bib}

\end{document}
