import argparse
import json
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('--input')
parser.add_argument('--output')
args = parser.parse_args()

with open(args.input, 'r') as input_file:
    results = np.array(json.load(input_file))
δxs, errors = results[:, 0], results[:, 1]

fig, axes = plt.subplots()
axes.set_xscale('log')
axes.set_yscale('log')
axes.plot(δxs, errors)
fig.savefig(args.output, bbox_inches='tight')
