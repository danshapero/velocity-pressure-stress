import argparse
import firedrake
from firedrake import assemble, energy_norm, Constant, sqrt, inner, grad, dx
import json
import numpy as np
from numpy import random, pi as π
from scipy.special import gamma as Γ
import tqdm
from trust_region import TrustRegionSolver

parser = argparse.ArgumentParser()
parser.add_argument('--degree', type=int, default=1)
parser.add_argument('--metric-smoothing-length', type=float)
parser.add_argument('--nmin', type=int, default=1)
parser.add_argument('--nmax', type=int, default=5)
parser.add_argument('--radius', type=float, default=1.0)
parser.add_argument('--exponent', type=float, default=4.0)
parser.add_argument('--solution-max', type=float, default=1.0)
parser.add_argument('--output')
args = parser.parse_args()


def exact_solution(x, α, p, R):
    r = sqrt(inner(x, x))
    q = p / (p - 1)
    return (α / 2) ** (q - 1) * (R ** q - r ** q) / q


results = []
for N in tqdm.trange(args.nmin, args.nmax + 1):
    R = Constant(args.radius)
    disk = firedrake.UnitDiskMesh(N)
    Vc = disk.coordinates.function_space()
    coords = firedrake.interpolate(R * disk.coordinates, Vc)
    mesh = firedrake.Mesh(coords)
    Q = firedrake.FunctionSpace(mesh, 'CG', args.degree)

    p = Constant(args.exponent)
    q = Constant(p / (p - 1))
    α = 2 * Constant(q * args.solution_max) ** (p - 1) / R ** p
    f = Constant(α)

    x = firedrake.SpatialCoordinate(mesh)
    u_true = firedrake.interpolate(exact_solution(x, α, p, R), Q)

    def objective(u):
        return (1 / p * inner(grad(u), grad(u))**(p / 2) - f * u) * dx

    # Create the initial guess for u by solving the linear Poisson equation
    u = firedrake.Function(Q)
    J = (0.5 * inner(grad(u), grad(u)) - f * u) * dx
    bc = firedrake.DirichletBC(Q, 0, 'on_boundary')
    firedrake.solve(firedrake.derivative(J, u) == 0, u, bc)

    # Create the metric defining the length of a proposal step
    φ, ψ = firedrake.TestFunction(Q), firedrake.TrialFunction(Q)
    if args.metric_smoothing_length is not None:
        λ = Constant(args.metric_smoothing_length)
    else:
        area = assemble(Constant(1) * dx(mesh))
        d = mesh.geometric_dimension()
        λ = Constant((Γ(d / 2 + 1) * area)**(1 / d) / np.sqrt(π))

    metric = (φ * ψ + λ ** 2 * inner(grad(φ), grad(ψ))) * dx

    radius = np.sqrt(assemble(energy_norm(metric, u)))
    bc = firedrake.DirichletBC(Q, 0, 'on_boundary')
    solver = TrustRegionSolver(u, objective, metric, radius, bc)

    for step in range(20):
        solver.step()

    u = solver.solution
    δx = mesh.cell_sizes.dat.data_ro[:].min()
    error = assemble(abs(u - u_true)**p * dx) ** (1 / float(p))
    results.append((δx, error))

with open(args.output, 'w') as output_file:
    output_file.write(json.dumps(results))
