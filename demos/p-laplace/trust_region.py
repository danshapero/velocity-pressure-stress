import firedrake
from firedrake import (
    action, assemble, Constant, derivative, energy_norm, replace
)
import numpy as np
import scipy.optimize

class TrustRegionSolver:
    def __init__(
        self,
        initial_guess,
        objective,
        metric,
        initial_radius,
        bcs,
        acceptance_threshold=1/8,
        length_epsilon=1e-2,
        max_radius=None,
        solver_parameters=None,
        form_compiler_parameters=None,
    ):
        self._u = initial_guess.copy(deepcopy=True)
        self._v = firedrake.Function(self._u.function_space())

        self._objective = objective
        self._metric = metric

        self._radius = initial_radius
        self._multiplier = Constant(1.)

        self._bc = bcs
        self._bc.homogenize()

        u, v = self.solution, self.proposal_step
        λ, Δ = self.multiplier, self.trust_region_radius

        J = self._objective(u)
        F = derivative(J, u)
        H = derivative(F, u)
        M = self._metric
        K = H + λ * M

        problem = firedrake.LinearVariationalProblem(
            K, -F, v, self._bc, form_compiler_parameters=form_compiler_parameters
        )
        defaults = {
            'ksp_type': 'preonly',
            'pc_type': 'lu',
            'pc_factor_mat_solver_type': 'mumps'
        }
        solver = firedrake.LinearVariationalSolver(
            problem, solver_parameters=solver_parameters or defaults
        )
        self._proposal_step_solver = solver

        self._threshold = acceptance_threshold
        self._length_eps = length_epsilon
        # I have no idea if this makes sense
        if max_radius is None:
            length_u = np.sqrt(assemble(energy_norm(M, u)))
            self._max_radius = 1e3 * max(length_u, initial_radius)
        else:
            self._max_radius = max_radius

        self.compute_proposal_step()

    @property
    def solution(self):
        return self._u

    @property
    def proposal_step(self):
        return self._v

    @property
    def trust_region_radius(self):
        return self._radius

    @property
    def multiplier(self):
        return self._multiplier

    def compute_proposal_step(self):
        u = self.solution
        v, M = self.proposal_step, self._metric
        λ, Δ = self.multiplier, self.trust_region_radius
        self._proposal_step_solver.solve()

        def f(s):
            λ.assign(s)

            try:
                self._proposal_step_solver.solve()
            # This is paranoia and we should log this if it happens
            except firedrake.ConvergenceError:
                return - 1 / Δ

            return 1 / np.sqrt(assemble(energy_norm(M, v))) - 1 / Δ

        # If the length of the proposal step is already less than the trust
        # region radius, we're done.
        if f(0.0) >= 0.0:
            return

        # Find a bracketing interval for the Lagrange multiplier.
        λ_max = 1.0
        while f(λ_max) < 0:
            λ_max *= 2

        # Do some root-finding to compute the multiplier.
        result = scipy.optimize.root_scalar(f, bracket=[0.0, λ_max])
        assert result.converged
        λ.assign(result.root)
        self._proposal_step_solver.solve()

    def step(self):
        u, v = self.solution, self.proposal_step
        M, Δ = self._metric, self.trust_region_radius
        length = np.sqrt(assemble(energy_norm(M, v)))

        J = self._objective(u)
        actual_decrease = assemble(J - replace(J, {u: u + v}))
        F = derivative(J, u)
        H = derivative(F, u)
        predicted_decrease = -assemble(action(F, v) + 0.5 * energy_norm(H, v))
        ρ = actual_decrease / predicted_decrease

        # See algorithm 4.1 in Nocedal and Wright
        η, ε, Δ_max = self._threshold, self._length_eps, self._max_radius
        if ρ < 1/4:
            self._radius = Δ / 4
        elif ρ > 3/4 and length >= (1 - ε) * Δ:
            self._radius = min(2 * Δ, Δ_max)

        if ρ > η:
            u.assign(u + v)

        self.compute_proposal_step()
